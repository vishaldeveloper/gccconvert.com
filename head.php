<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>gccconvert</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <!-- <link href="assets/img/favicon.png" rel="icon"> -->
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v2.3.1
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style type="text/css">
    #footer .footer-top {
    background: #35496a;
    color: #fff;
    }
    #footer .footer-top .footer-contact p ,
    #footer .footer-top .footer-links ul a ,
    #footer .footer-top .footer-contact h3 ,
    #footer .footer-top h4 ,
    #pricing p ,
    #pricing h2 ,
    .logo{
    color: #fff;
    }

    #pricing{
    background: #37517e;
    }

    #header.header-scrolled, #header.header-inner-pages {
        background: #35496a;
    }
</style>
<?php

$soft_constant = array(
  'Quick book' => 'Quick Book',
  'wove' => 'Wove',
  'QBO' => 'QBO',
  'Zoho' => 'Zoho',
  'Reckon' => 'Reckon',
  'Reckon one' => 'Reckon one',
  'Free agent' => 'Free agent',
  'Kash flow' => 'Kash flow',
  'Excel' => 'Excel',
  'any software' => 'any software',
);
define("SOFTWARE_CONSTANT", $soft_constant);

$country_constant = array(
  'India' => 'India',
);
define("COUNTRY_CONSTANT", $country_constant);


$plan_constant = array(
  'indian_rupee' => 'Rupee',
  'usa_dollar' => 'Dollar',
);
define("CURRENCY_CONSTANT", $plan_constant);


$plan_constant = array(
  'Basic' => 'Basic',
  'Transfer' => 'Transfer',
);
define("PLAN_CONSTANT", $plan_constant);

?>
</head>