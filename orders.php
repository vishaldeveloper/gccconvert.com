<?php
include('header_new.php');
?>

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <!-- <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.html">Home</a></li>
          <li>Inner Page</li>
        </ol>
        <h2>Orders</h2>

      </div>
    </section> -->
    <!-- End Breadcrumbs -->

    <section class="inner-page">
      <div class="container">
          <!-- ***** -->
           <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
          <div class="container" data-aos="fade-up">

            <div class="section-title">
              <h2>Orders</h2>
              <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
            </div>

            <div class="row">

              <!-- <div class="col-lg-5 d-flex align-items-stretch">
                <div class="info">
                  <div class="address">
                    <i class="icofont-google-map"></i>
                    <h4>Location:</h4>
                    <p>A108 Adam Street, New York, NY 535022</p>
                  </div>

                  <div class="email">
                    <i class="icofont-envelope"></i>
                    <h4>Email:</h4>
                    <p>info@example.com</p>
                  </div>

                  <div class="phone">
                    <i class="icofont-phone"></i>
                    <h4>Call:</h4>
                    <p>+1 5589 55488 55s</p>
                  </div>

                  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
                </div>
              </div> -->
              <div class="col-lg-3 d-flex align-items-stretch">
              </div>
              <div class="col-lg-6 mt-5 mt-lg-0 d-flex align-items-stretch">
                <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="name">Software</label>
                      <select class="form-select form-control" id="select_software" name="select_software">
                        <option selected>Select</option>
                         <?php
                          foreach (SOFTWARE_CONSTANT as $key_soft => $value_soft) {
                            ?>
                            <option value="<?php echo $key_soft ; ?>"><?php echo $value_soft ; ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="name">Country</label>
                      <select class="form-select form-control" id="select_country" name="select_country">
                        <option selected>Select</option>
                         <?php
                          foreach (COUNTRY_CONSTANT as $key_country => $value_country) {
                            ?>
                            <option value="<?php echo $key_country ; ?>"><?php echo $value_country ; ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="name">Currency</label>
                      <select class="form-select form-control" id="select_currency" name="select_currency">
                        <option selected>Select</option>
                         <?php
                          foreach (CURRENCY_CONSTANT as $key_currency => $value_currency) {
                            ?>
                            <option value="<?php echo $key_currency ; ?>"><?php echo $value_currency ; ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="name">Plan</label>
                      <select class="form-select form-control" id="select_plan" name="select_plan">
                        <option selected>Select</option>
                         <?php
                          foreach (PLAN_CONSTANT as $key_plan => $value_plan) {
                            ?>
                            <option value="<?php echo $key_plan ; ?>"><?php echo $value_plan ; ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="name">Upload File</label>
                      <input type="file" class="form-control" id="upload_file" name="upload_file">
                    </div>
                    <p class="form-group col-md-12">OR</p>
                    

                    <div class="form-group col-md-6">
                      <label for="name">ID</label>
                      <input type="text" class="form-control" id="file_id" name="file_id">
                    </div>

                    <div class="form-group col-md-6">
                      <label for="name">Password</label>
                      <input type="password" class="form-control" id="file_password" name="file_password">
                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="name">Your Name</label>
                        <input type="text" name="name" class="form-control" id="name" name="full_name"  />
                        <div class="validate"></div>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="name">Your Contact</label>
                        <input type="text" class="form-control" name="contact_no" id="contact_no"  />
                        <div class="validate"></div>
                      </div>
                      <div class="form-group col-md-12">
                        <label for="name">Your Email</label>
                        <input type="email" class="form-control" name="email" id="email" />
                        <div class="validate"></div>
                      </div>
                       
                    </div>


                   
                  <div class="mb-3">
                    <div class="loading">Loading</div>
                    <div class="error-message"></div>
                    <div class="sent-message">Your message has been sent. Thank you!</div>
                  </div>
                  <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
              </div>

            </div>

          </div>
        </section><!-- End Contact Section -->
          <!-- ***** -->
      </div>
    </section>

  </main><!-- End #main -->

<?php
  include('footer.php');
?>